console.log('Hello World');

// function printInfo(){
//     let nickname = prompt('Enter your nickname: ');
//     console.log('Hi, '+ nickname);
// };

// printInfo(); /* invoke */

                    /* parameter */
function printName(firstName){
    console.log('My name is ' + firstName);
}

printName('Juana'); /* argument */
printName('John');
printName('Cena');

// Now we have a reusable function / reusable task that could have different output based on what the value to process, with the help of parameters and arguments.

// [SECTION] Parameters and Arguments

// Parameter
    // 'firstNAme' is called a parameter.
    // A parameter acts as named variable/container that exists only inside a function.
    // Its used to store information that is provided to a function whent it's called/invoked.

// Argument
    // 'Juana', 'John', and 'Cena' the information/data provided directly into the function is called 'argument.'
    // Values passed when invoking a function are called arguments.
    // These arguments are then stored as the parameter within the function.


let sampleVariable = 'Yui';

printName(sampleVariable);

// Variables can also be passed as an argument.

/* ---------------------------------------------------------------------------------------------------------- */

function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log('The remainder of ' + num + ' divided by 8 is: ' + remainder);
    let isDivisibilityBy8 = remainder === 0;
    console.log('Is ' + num + ' divisible by 8?');
    console.log(isDivisibilityBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);


// [SECTION] Function as argument
    // Function parameters can also accept functions are arguments.
    // Some complex functions uses other functions to perform more complicated results.

    function argumentFunction(){
        console.log('This function was passed as an argument before the message was printed.');
    };

    function invokeFunction(argumentFunction){
        argumentFunction();
    };

    invokeFunction(argumentFunction);
    console.log(argumentFunction);

/* ---------------------------------------------------------------------------------------------------------- */

// [SECTION] Using Multiple Parameters

    function createFullName(firstName, middleName, lastName){
        console.log('My full name is ' + firstName + ' ' + middleName + ' ' + lastName);
    }

    createFullName('John', 'Albert', 'Escalada'); /* follows order of parameters respectively */

    // Using variables as an argument

    let firstName = 'John';
    let middleName = 'Doe';
    let lastName = 'Smith';

    createFullName(firstName, middleName, lastName);

    function getDifferenceof8Minus4(numA, numB){
        console.log('Difference: ' + (numA-numB));
    }

    getDifferenceof8Minus4(8,4);
    getDifferenceof8Minus4(4,8); /* This will result to logical error */

/* ---------------------------------------------------------------------------------------------------------- */

// [SECTION] Return Statement

    // The return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called.

    function returnFullName(firstName, middleName, lastName){
        // return firstName + ' ' + middleName + ' ' + lastName;

        // We could also create a variable inside the function to contain the result and return the variable instead.
        let fullName = firstName + ' ' + middleName + ' ' + lastName;
        return fullName;

        // codes after return code will not work and will not be printed
        console.log('This is printed insde a function');
    }

    let completeName = returnFullName('Paul', 'Smith', 'Jordan');
    console.log(completeName);

    console.log('I am ' + completeName);


    function printPlayerInfo(userName, level, job){
        console.log('Username: ' + userName);
        console.log('Level: ' + level);
        console.log('Job: ' + job);
    };

    let user1 = printPlayerInfo('JubZ', 'Grandmaster', 'WebDev');
    console.log(user1); /* returns undefined */